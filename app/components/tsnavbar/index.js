'use strict';

var angular = require('angular');

/**
 * Directive and service for displaying a warning or info notification
 */
module.exports = angular.module('app.components.tsnavbar', [
  require('tssearch').name
  ])
  .directive('tsNavbar', function() {
    function link(scope, element, attrs) {      
      
    }
    return {
      restrict: 'E',
      template: require('./template.html'),
      controller: 'tsNavbarCtrl',
      link: link
    };
  })
  .controller('tsNavbarCtrl', require('./controller'));
