'use strict';

var moment = require('moment');
var $ = require('jquery');
var d3 = require('d3');
let paginationPageSizes = require('../../../values.js').paginationSizes;

/**
 * @ngInject
 */
module.exports = class ExpansionRatingComponent {
    constructor($rootScope, $state, $stateParams, $scope, $location, tsAuthService,
    promiseTracker, contextService, $window, $timeout, $animate, $interval, uiGridConstants) {
    'ngInject';

    var accountId = $stateParams.accountId;
    $scope.accountId = accountId;

    // Events
    $scope.expansionTracker = promiseTracker();

    $scope.gridExpansionRatingOptions = {
        data: [],
        paginationPageSize: 100,
        paginationPageSizes: paginationPageSizes.sizes,
        enablePaginationControls: true,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        isCollapsed: false,
        enableCellEditOnFocus: true,
        columnDefs: [
            {
                displayName: 'Month',
                field: 'plan_month',
                // cellFilter: 'date: "yyyy-MM-dd"',
                // cellTemplate: '<span class="ui-grid-contents">{{moment.utc(row.entity.plan_month).format(\'YYYY-MM\')}}</span>',
                minWidth: 80
            },{            
                displayName: 'Priority',
                field: 'priority',
                cellFilter: 'number: 0',
                minWidth: 80
            },{
                displayName: 'ETP Level',
                headerTooltip: 'ETP at the right level',
                field: 'etp_level',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Executive Commitment',
                headerTooltip: 'Executive Commitment to Apigee',
                field: 'executive_commitment',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Vision',
                headerTooltip: 'Magellan Vision Score',
                field: 'magellan_vision_score',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Execution',
                headerTooltip: 'Magellan Execution Score',
                field: 'magellan_execution_score',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Alignment',
                headerTooltip: 'Magellan Alignment Score',
                field: 'magellan_alignment_score',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Vision S',
                headerTooltip: 'Magellan Vision Sentiment',
                field: 'magellan_vision_sentiment',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Execution S',
                headerTooltip: 'Magellan Execution Sentiment',
                field: 'magellan_execution_sentiment',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Alignment S',
                headerTooltip: 'Magellan Alignment Sentiment',
                field: 'magellan_alignment_sentiment',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Sales Team',
                headerTooltip: 'Sales Team Engagement',
                field: 'sales_engagement',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Tech',
                headerTooltip: 'Tech Capability',
                field: 'tech_capability',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'API Traffic',
                headerTooltip: 'API Traffic Trend',
                field: 'api_traffic',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'API Count',
                headerTooltip: 'API Count Trend',
                field: 'api_count',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Dev Count',
                headerTooltip: 'API Developer Count Trend',
                field: 'api_dev_count',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'License',
                headerTooltip: 'Current License Value',
                field: 'license_value',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'License Opportunity',
                headerTooltip: 'Future License Value (12m)',
                field: 'new_license_value',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Deal Timing',
                headerTooltip: 'New Deal Timing',
                field: 'deal_timing',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'Budget',
                headerTooltip: 'Budget Sentiment',
                field: 'budget',
                cellFilter: 'number: 0',
                minWidth: 50
            },{
                displayName: 'GCP',
                headerTooltip: 'GCP Growth Sentiment',
                field: 'gcp_growth',
                cellFilter: 'number: 0',
                minWidth: 50
            }
        ],
        onRegisterApi: function(gridApi) {
            $animate.enabled(gridApi.grid.element, false);
            //set gridApi on scope
            $scope.gridApi = gridApi;

            $interval($scope.gridApi.core.handleWindowResize, 500, 10);

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                //Do your REST call here via $http.get or $http.post
                if(newValue  == ''){
                    newValue = null;
                }
                if(newValue == null){
                    rowEntity[colDef.field] = null;
                }else{
                    if(isNaN(parseInt(newValue)) || parseInt(newValue) < 0 || parseInt(newValue) > 10 ){
                        rowEntity[colDef.field] = oldValue;
                        alert('Invalid Value. Required to be 0-10.');
                        return;
                    }else{
                        newValue = parseInt(newValue);
                        rowEntity[colDef.field] = newValue;                        
                    }
                }
                contextService.updateExpansionRating(rowEntity.id, colDef.field, newValue);
                
                //alert('#TODO Update '+colDef.field+'['+rowEntity.id+']: '+oldValue+' -> '+newValue);
                
                //Alert to show what info about the edit is available
                // alert('Column: ' + colDef.name + ' ID: ' + rowEntity.id + ' Name: ' + rowEntity.name + ' Age: ' + rowEntity.age);
            });
        }
    };


    var expansionRatings = contextService.getAccountExpansionRatings(accountId)
        .then(list => {
            _.each(list, function(plan){
                plan.plan_month = moment.utc(plan.plan_month).format('YYYY-MM');
            });
            $scope.gridExpansionRatingOptions.data = list;
        });

    $scope.expansionTracker.addPromise(expansionRatings);
    
    $timeout(function(){
        var tab = $('#overviewTab');
        tab.removeClass('active');
    }, 250);
    }
};