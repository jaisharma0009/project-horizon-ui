'use strict';

var moment = require('moment');
var _ = require('lodash');
var url = require('url');
var approx = require('approximate-number');

function isEmpty(str) {
  if (str == null || str == undefined) return true;
  if (str.trim() == '') return true;
}



/**
 * @ngInject
 */
module.exports = function ($stateParams, $timeout, Restangular, csRestangular, promiseTracker, $q, $sce, $filter) {
  var self = this;

  this.getMockLogo = () => {
    var mockLogos = [
      'https://i.kinja-img.com/gawker-media/image/upload/s--pEKSmwzm--/c_scale,fl_progressive,q_80,w_800/1414228815325188681.jpg',
      'https://maxcdn.icons8.com/Share/icon/Logos//google_logo1600.png',
      'https://apigee.com/api-management/ui/images/224x104_fullcolor_header_opt.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2017-07/HP_Logos_Fujitsu_FC_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2017-07/HP_Logos_walgreens_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2016-09/HP_Logos_BBCWorldwide_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2016-09/HP_Logos_TheWorldBank_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2016-09/HP_Logos_Swisscom_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2016-09/Deeper_Dive_Code_0.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2017-07/HP_Logos_staples.png',
      'https://live-hl-apigeecom.devportal.apigee.com/sites/default/files/2017-07/HP_Logos_Markspencer.png',
      'http://customerlogos.images.www-static.apigee.s3-website-us-west-2.amazonaws.com/internaluse/dontexist'
    ]

    return mockLogos[Math.floor(Math.random() * mockLogos.length)];
  }

  this.generateStepsGraph = function(index, total) {
    let stageSteps = "";

    for(let i=0; i < total; i++) {
      let inner = (i == index - 1) ? '<div class="bubble-inner"></div>' : '';
      stageSteps += `<div class="bubble">${inner}</div>`;
    }

    return `<div class="stage">${stageSteps}</div>`;
  }

  this.getOpportunityTypes = function() {

    return Restangular
      .all('cs/opportunity_types')
      .getList()
      .then(list => {
        return list;
      });

  }

  this.getOpportunitiesByQuarterList = function (amount, probability, is_current, opportunity_type) {

    var where = [
      'closed:ne:true',
    ];

    if (amount != null) where.push('amount:gt:' + amount);
    if (probability != null) where.push('probability:gte:' + probability);
    if (is_current) where.push('account_sales_stage:eq:6 - Customer');
    if (opportunity_type) where.push('opportunity_type:eq:'+ opportunity_type);


    var promise = Restangular
      .all('cs/opportunities')
      .getList({
        where: where,
        orderby: [
          'opportunity_type:ASC',
          'fq:asc',
          'amount:desc'
        ]
      })
      .then(list => {
        _.each(list, opp => {
          // opp['account_logo'] = 'http://customerlogos.images.www-static.apigee.s3-website-us-west-2.amazonaws.com/internaluse/' + opp.account_id;
          if (opp.website) {
            var parsed = url.parse(opp.website);
            if (parsed.hostname)
              opp.website = parsed.hostname;
          }
          opp['account_logo'] = '//logo.clearbit.com/' + opp['website'] + '?size=60';
          opp['approx_amount'] = approx(opp.amount.replace(/,/g, ''));
          if (ENV == 'local') {
            opp['account_logo'] = self.getMockLogo();
          }
          //opp.next_steps = $sce.trustAsHtml(parsePopup(opp));
          if(isEmpty(opp.next_steps))
            opp.next_steps = [];
          else
            opp.next_steps = opp.next_steps.split('\n');

          let stageNum = opp.sales_stage.match(/\d+/)[0];
          opp['stage'] = stageNum;

          var percentage = stageNum * 100 / 6;
          percentage =  Math.round(percentage/5)*5;
          opp['percentage'] = percentage;
        });

        var ret = {};
        list = _.groupBy(list, 'opportunity_type');
        _.each(list, function (group, opportunity_type) {
          group = _.groupBy(group, 'fq');
          ret[opportunity_type] = group;
        });
        return ret;
      })
      ;

    return promise;

  };

};
