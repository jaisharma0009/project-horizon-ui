'use strict';

import $ from 'jquery';
import scrollbar from 'jquery.scrollbar';

export class ScrollbarDirective {
  attribute = 'scrollbar';
  restrict = 'A';

  constructor($rootScope, $state) {
    'ngInject';
  }

  link(scope, element, attrs) {
    $(document).ready(() => {
      $(element).addClass('c-scrollbar');
      $(element).scrollbar();
    });
  }
  
}
