'use strict';

/**
 * @ngInject
 */
module.exports = class TsAlertsController {
  constructor($scope, tsAlertsService) {
    this.$scope = $scope;
    this.tsAlertsService = tsAlertsService;
  }

  $onInit() {
    this.$scope.tsAlerts = this.tsAlertsService;
  }
};
