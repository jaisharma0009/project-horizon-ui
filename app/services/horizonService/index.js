'use strict';

var angular = require('angular');

module.exports = angular.module('app.customers.services.horizonService', [])
  .service('HorizonService', require('./service'));