'use strict';

var angular = require('angular'),
  $ = require('jquery'),
  footable = require('footable-shim');

/**
 * Directives for adding FooTable functionality to a table
 */
module.exports = angular.module('app.components.jqfootable', [])
  /**
   * Directive for initializing FooTable on a <table> element
   */
  .directive('jqFootable', function() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        var options = scope.$eval(attrs.jqFootable);

        scope.$evalAsync(function() {
          $(elem).footable(options);
        });
      }
    };
  })
  /**
   * Directive for redrawing FooTable when the expression passed into
   * the attribute changes (e.g. jq-footable-refresh="variable")
   */
  .directive('jqFootableRefresh', function($parse) {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        scope.$watch(attrs.jqFootableRefresh, function(newValue) {
          var elemFootable = $(elem).data('footable');

          if (newValue && elemFootable) {
            scope.$evalAsync(function() {
              elemFootable.redraw();
            });
          }
        }, true);
      }
    };
  });
