'use strict';

var _ = require('lodash'),
  $ = require('jquery');

module.exports = class ModelsController {
  constructor($scope, contextService, $location, $window, $rootScope) {
    'ngInject';

    this.$scope = $scope;
    this.contextService = contextService;
    this.$location = $location;
    this.$window = $window;
    this.$rootScope = $rootScope;
    this.models = [];
    this.displayImport = false;
    this.gridChanged;
    this.selectedModel;
  }

  $onInit() {
    $('#models-container').on('click', e => { e.stopPropagation() });
    $(document).one('click', e => { this.clickOutside() });
    this.getModels();
    this.selectModel({id: 'all', name: 'all', shortdesc: 'All'}, true);

    this.$scope.$on('GRID_CHANGED', (e, data) => {
      if(!!data.entity) {
        this.gridChanged = data.changed;
      } else {
        this.gridChanged = false;
      }
    });

    this.$scope.$on('MODEL_SAVED', this.getModels.bind(this));
    this.$scope.$on('MODEL_SAVED_ERROR', this.getModels.bind(this));

    this.$rootScope.$on('NEW_MODEL', (e, model) => {
      model = this.transformObj(model);
      this.models.push(model);
      this.selectedModel = { id: model.id, name: model.shortdesc };
    })
  }

  transformObj(obj) {
    if(typeof obj == 'string') {
      return JSON.parse(obj);
    } else {
      return obj;
    }
  }

  getImportingId() {
    let modelId = this.$location.search().model;
    if(modelId) {
      return modelId;
    } else {
      return null;
    }
  }

  bindClickOutside() {
    $(document).one('click', this.clickOutside);
  }

  clickOutside() {
    this.modelsPanelVisible = false;
    $(document).one('click', () => {
      this.clickOutside();
    });
  }

  getModels() {
    this.contextService.getExplansionPlanningModels()
      .then(models => {

        if(ENV == 'local') {
          models = _.filter(models, m => {
            return !m.user_id || m.user_id == 1;
          });
        }

        let mod = _.map(models, p => {
          p.shareLink = this.getShareLink(p);
          p.tmpShortdesc = p.shortdesc;

          if(!p.user_id) {
            p.user_id = "";
          }

          return p;
        });

        this.models = _.sortBy(mod, p => p.user_id);

        _.each(this.models, (p, i) => {
          if(i > 0) {
            if(!!p.user_id && !this.models[i-1].user_id) {
              this.models[i-1].lastPublic = true;
            }
          }
        })

        if(!localStorage.getItem('selectedModel')) {
          this.selectModel({id: 'all', name: 'all', shortdesc: 'All'});
        } else {
          let sel = localStorage.getItem('selectedModel');
          sel = JSON.parse(sel);

          let index = _.findIndex(this.models, i => i.id == sel.id);

          this.selectModel(this.models[index]);
        }

        let saving = this.contextService.getExpansionPlanningSaving();
        if(saving) {
          let sIndex = _.findIndex(this.models, i => {
            return i.shortdesc.toLowerCase() == saving.toLowerCase();
          });

          this.selectModel(this.models[sIndex]);
          this.contextService.clearExpansionPlanningSaving();
        }

      });
  }

  selectModel(model, init) {
    this.gridChanged = false;
    this.$rootScope.$broadcast('MODEL_SELECTED', model);
    this.modelsPanelVisible = false;
    this.selectedModel = { id: model.id, name: model.shortdesc };

    if(this.selectedModel.id != 'all') {
      localStorage.setItem('selectedModel', JSON.stringify(this.selectedModel));
    } else {
      if(!init) {
        localStorage.removeItem('selectedModel');
      }
    }
  }

  getShareLink(model) {
    let url = this.$location.absUrl().split('?')[0];
    return `${url}?model=${model.id}`;
  }

  removeModel(model) {
    let index = _.findIndex(this.models, p => p.id == model.id);
    this.models.splice(index, 1);
    this.contextService.deleteExpansionPlanningModel(model.id);
  }

  saveModel(name) {
    this.$rootScope.$broadcast('SAVE_MODEL', name);
    this.saveName = null;
    this.cancelImport();
  }

  updateModel(model) {
    this.gridChanged = false;

    if(!model) {
      let index = _.findIndex(this.models, p => p.id == this.selectedModel.id);
      model = this.models[index];
    }

    model.shortdesc = model.tmpShortdesc;
    this.contextService.updateExpansionPlanningModel(model.id, model.shortdesc, model);
    model.editing = false;
    this.cancelImport();
  }

  importModel(id) {
    this.contextService.getExpansionPlanningModel(id).then(
      model => {
        this.contextService.saveExpansionPlanningModel(model.shortdesc, model.entity);
        this.displayImport = false;
        this.importId = null;
      }
    )
  }

  cancelImport() {
    this.displayImport = false;
    this.displaySave = false;
    this.importId = null;
    this.saveName = null;
  }

  toggleModelsPanel() {
    this.modelsPanelVisible = !this.modelsPanelVisible;
  }

  copyShareUrl(model) {
    let toCopy = model.shareLink;

    let body = angular.element(this.$window.document.body);
    let textarea = angular.element('<textarea/>');
    textarea.css({
      position: 'fixed',
      opacity: '0'
    });

    textarea.val(toCopy);
    body.append(textarea);
    textarea[0].select();

    try {
      let successful = document.execCommand('copy');
      if (!successful) throw successful;
      alert('Link has been copied to clipboard!');
    } catch (err) {
      window.prompt("Copy to clipboard: Ctrl+C, Enter", toCopy);
    }

    textarea.remove();
  };

}
