'use strict';

let _ = require('lodash'),
    $ = require('jquery');

/**
 * @ngInject
 */
module.exports = function($scope, $element, $state) {
  $scope.hasBackLink = hasBackLink();
  $scope.$on('SCROLL_BOTTOM', nextSection);

  $(document).ready(() => {
    $($element).find('.hamburguer').click(() => {
      $($element).find('.sidebar-links').slideToggle();
    });
  });

  function nextSection() {
    let sections = $scope.options.sections;
		let current = $state.current.name;

    let index = _.findIndex(sections, { state: current});

    let goIndex = index == sections.length - 1 ? 0 : index +1;
    $state.go(sections[goIndex].state);
		document.body.scrollTop = 0;
	}
  
  function hasBackLink() {
    let backLinkOpts = $scope.options.backLink;

    if(backLinkOpts) {
      if(!backLinkOpts.name || !backLinkOpts.state) {
        return false;
      }

      return true
    }

    return false;
  }
};