'use strict';

var angular = require('angular');
var $ = require('jquery');
var template = require('./template.html');
var uib = require('angular-ui-bootstrap');

module.exports = angular.module('app.components.kanbanbadge', ['ui.bootstrap'])
  .directive('kanbanBadge', function ($rootScope, $sce) {
    return {
      restrict: 'E',
      scope: {
        value: '@',
        title: '@',
        description: '@',
        color: '@',
        badge: "=",
        ngValue: '=',
        ngDescription: '=',
        ngTitle: '=',
        ngColor: '='
      },
      template: template,
      link: function (scope, element, attrs, ctrl) {
        let value = scope.value || scope.ngValue;
        let title = scope.title || scope.ngTitle;
        let description = scope.description || scope.ngDescription;
        let color = scope.color || scope.ngColor;

        if(!!scope.badge) {
          value = scope.badge.value || value;
          title = scope.badge.title || title;
          description = scope.badge.description || description;
          color = scope.badge.color || color;
        }

        scope.value = value;
        scope.color = color;

        let colorStyle = !!color ? colorStyle = `background-color: ${color}` : "";

        let content = 
          `<div>
            <div>
              <span style="${colorStyle}" class="kb-badge ${value}"></span> ${title}
            </div>
            <div>
              ${description}
            </div>
          </div>`;

        scope.popoverContent = $sce.trustAsHtml(content);
      }
    };
  });
