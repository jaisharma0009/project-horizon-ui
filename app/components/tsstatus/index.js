'use strict';

var angular = require('angular');

/**
 * Directive for displaying a stoplight-style status indicator
 */
module.exports = angular.module('app.components.tsstatus', [])
  .directive('tsStatus', function() {
    return {
      restrict: 'E',
      scope: {
        status: '='
      },
      template: require('./template.html'),
      controller: 'tsStatusCtrl'
    };
  })
  .controller('tsStatusCtrl', require('./controller'));
