var template = require('./explore.template.html');
var ExploreController = require('./explore.controller');

let ExploreComponent = {
  restrict: 'E',
  template,
  controller: ExploreController,
  controllerAs: 'explore'
};

module.exports = ExploreComponent;