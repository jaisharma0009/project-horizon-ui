let template = require('./template.html');
let TechnicalCompetencyController = require('./controller');

let TechnicalCompetencyComponent = {
  restrict: 'E',
  template,
  controller: TechnicalCompetencyController,
  controllerAs: 'es'
};

module.exports = TechnicalCompetencyComponent;