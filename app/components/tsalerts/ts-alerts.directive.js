'use strict';

export default class TsAlertsDirective {
  constructor() {}

  restrict = 'E';
  transclude = true;
  template = require('./template.html');
  controller = require('./controller');
}