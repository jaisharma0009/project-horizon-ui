'use strict';

/**
 * @ngInject
 */
module.exports = function (Restangular, $q) {
  this.internalFilter = false;

  this.getInternalFilter = () => this.internalFilter;

  this.disableInternalFilter = () => {
    this.internalFilter = false;
  }

  this.enableInternalFilter = () => {
    this.internalFilter = true;
  }

  this.getEngageOpportunities = () => {
    let endpoint = 'sfdc/horizon_engage_score_views';
    let qp = {};

    if(!this.internalFilter) {
      qp = { filter_by_user: true };
    }

    return Restangular
            .all(endpoint)
            .getList(qp)
            .then(list => list);
  }

  this.getEngageOpportunity = account_id => {
    var endpoint = 'sfdc/horizon_engage_score_views/account_id';

    if(ENV == 'local') {
      endpoint = 'sfdc/horizon_engage_score_views';
    }
    
    let qp = {
      where: 'account_id:eq:' + account_id, 
      account_id: account_id
    }

    if(ENV == 'local') {
      return Restangular
        .all(endpoint)
        .getList(qp)
        .then(opp => opp);  
    }

    return Restangular
      .one(endpoint, account_id)
      .get(qp)
      .then(opp => opp);

  }

  this.getUnassociatedTrialUsers = account_id => {
    var endpoint = 'sfdc/unassociated_trial_users';
    return Restangular.all(endpoint).getList(list => list);
  }

  this.getUnassociatedWebActivity = account_id => {
    var endpoint = '/sfdc/unassociated_web_activity';
    return Restangular.all(endpoint).getList(list => list);
  }

  /**
   * Retreives the relevant info based on the account id or account name
   **/
  this.getRelevantInfo = account => {
    var endpoint = '';

    if(ENV == 'local') {
      endpoint = 'sfdc';
    }

    return Restangular
            .all(endpoint)
            .get('news',{ org: [account] })
            .then(info =>
              typeof info.results === 'string' ?
                { results: JSON.parse(info.results) } : info
            );
  }

  this.getMarketingInfo = account_id => {
    var endpoint = 'sfdc/horizon_engage_company_deep_dives';

    return Restangular
      .all(endpoint)
      .getList({ where: ['account_id:eq:' + account_id] })
      .then(opp => opp);

  }

  /** <EXPLORE> */
  function addWhereParams(region, quarter, year) {
    let where = [];

    if(!!region) {
      where.push(`region:eq:${region}`);
    }

    if(!!quarter) {
      where.push(`quarter:eq:${quarter}`);
    }

    if(!!year) {
      where.push(`year:eq:${year}`);
    }

    return where;
  }

  this.getOverviews = (region, quarter, year) => {
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);
    
    return Restangular.all('sfdc')
            .get('horizon_explore_overviews', qp)
            .then(data => data)
  }


  this.getTopCampaings = (region, quarter, year) => {    
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);

    if(ENV == 'local') {
      qp.where = [];
    }

    return Restangular.all('sfdc')
      .get('horizon_explore_campaign_score_views', qp)
      .then(data => data);
  }

  this.getEventsInfluenceSummary = (region, quarter, year) => {
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);

    return Restangular.all('sfdc')
              .get('horizon_explore_events_totals', qp)
              .then(data => data);
  }

  this.getEventsByType = (region, quarter, year) => {
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);

    return Restangular.all('sfdc')
                      .get('horizon_explore_events_by_types', qp)
                      .then(data => data)
  }

  this.getEventsByTypeClosed = (region, quarter, year) => {
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);

    return Restangular.all('sfdc')
                      .get('horizon_explore_events_by_type_closeds', qp)
                      .then(data => data)
  }

  this.getEventConvertionScore = (region, quarter, year) => {
    let qp = { where: [] };
    qp.where = addWhereParams(region, quarter, year);

    return Restangular.all('sfdc')
                      .get('horizon_explore_events_conversion_score_view', qp)
                      .then(data => data)
  }
  

  /** </EXPLORE> */
}
