'use strict';

let paginationPageSizes = require('../values.js').paginationSizes;
let customRow = require('../values.js').customRow;
let placeholderRow = require('../values.js').placeholderRow;
let customSort = require('../values.js').customSort;

module.exports = class EngageController {
  constructor($scope, $state, HorizonService, promiseTracker, $animate, $filter) {
    'ngInject';

    this.internalFilter = HorizonService.getInternalFilter();
    this.$scope = $scope;
    this.$animate = $animate;
    this.HorizonService = HorizonService;
    this.oppsTracker = promiseTracker();
    this.numberFilter = $filter('numberFilter');
    this.textFilter = $filter('myTextNullFilter');
    this.customRow = customRow('ui-sref="engage.details({accountId: row.entity.account_id})"');
    this.customSort = customSort;
    this.user = {};
  }

  $onInit() {
    this.grid = {
      paginationPageSize: 100,
      paginationPageSizes: paginationPageSizes.sizes,
      enablePaginationControls: true,
      data: [],
      enableFiltering: true,
      exporterCsvFilename: 'myFile.csv',
      enableGridMenu: false,
      exporterMenuPdf: false,
      enableColumnMenus: false,
      rowHeight: 30,
      rowTemplate: this.customRow,
      columnDefs: [
        {
          displayName: 'Account Name',
          name: 'account_name',
          field: 'account_name',
          minWidth: 130,
          cellTooltip: true,
          sortingAlgorithm: this.customSort,
          filter: { 'condition': this.textFilter },
          cellTemplate: `<div class="ui-grid-cell-contents"">
                          <span title="{{row.entity.account_name | htmldecode}}">{{row.entity.account_name | htmldecode}}</span>
                         </div>`
        },
         {
          displayName: 'Opportunity Name',
          name: 'opportunity_name',
          field: 'opportunity_name',
          minWidth: 165,
          sortingAlgorithm: this.customSort,
          cellTooltip: true,
          filter: { 'condition': this.textFilter },
          cellTemplate: `<div class="ui-grid-cell-contents">
                          <span title="{{row.entity.opportunity_name | htmldecode}}">{{row.entity.opportunity_name | htmldecode}}</span>
                         </div>`
        },
        {
          displayName: 'Stage',
          name: 'stage',
          field: 'sales_stage',
          cellTooltip: false,
          sortingAlgorithm: this.customSort,
          width: 90,
          filter: { 'condition': this.textFilter },
          cellTemplate: `<div class="ui-grid-cell-contents">
                          <span uib-tooltip="{{row.entity.sales_stage}}" tooltip-placement="right" tooltip-append-to-body="true">
                            {{row.entity.sales_stage | stageFilter:'short'}}
                          </span>
                        </div>`
        },
        {
          displayName: 'Exp. Close',
          name: 'exp_close',
          field: 'fiscal_period',
          cellTooltip: false,
          sortingAlgorithm: this.customSort,
          width: 98,
          cellClass: 'text-center',
          filter: { 'condition': this.textFilter },
          cellTemplate: `<div class="ui-grid-cell-contents" ng-bind-html="row.entity.fiscal_period | quarterFilter">
                         </div>`

        },
        {
          displayName: 'Opportunity Amount',
          name: 'opportunity',
          field: 'current_opportunity_amount',
          sortingAlgorithm: this.customSort,
          cellTooltip: true,
          minWidth: 173,
          cellClass: "text-center",
          filter: { 'condition': this.numberFilter },
          cellTemplate: `<span class="ui-grid-cell-contents" uib-tooltip="{{row.entity.current_opportunity_amount}}"
                          tooltip-placement="right" tooltip-append-to-body="true">
                          {{row.entity.current_opportunity_amount | approximate}}
                         </span>`
        },
        {
          displayName: 'Trial Status',
          name: 'Trial_status',
          field: 'trial_status',
          cellTooltip: false,
          sortingAlgorithm: this.customSort,
          width: 110,
          cellClass: "text-center",
          filter: { 'condition': this.textFilter },
          cellTemplate: `<span class="ui-grid-cell-contents {{row.entity.trial_status | engageStatusDotsClass}}"
                          uib-tooltip="{{row.entity.trial_status}}" tooltip-placement="right" tooltip-append-to-body="true"></span>`
        },
        {
          displayName: 'Mkt Engagement',
          name: 'marketing_engagement',
          field: 'marketing_engagement',
          cellTooltip: false,
          sortingAlgorithm: this.customSort,
          minWidth: 160,
          cellClass: "text-center",
          filter: { 'condition': this.textFilter },
          cellTemplate: `<span class="ui-grid-cell-contents {{row.entity.marketing_engagement | engageStatusDotsClass }}"
                          uib-tooltip="{{row.entity.marketing_engagement}}" tooltip-placement="right" tooltip-append-to-body="true"></span>`
        },
        {
          displayName: 'Web Engagement',
          name: 'web_engagement',
          field: 'website_engagement',
          sortingAlgorithm: this.customSort,
          cellTooltip: false,
          minWidth: 160,
          cellClass: "text-center",
          filter: { 'condition': this.textFilter },
          cellTemplate: `<span class="ui-grid-cell-contents {{row.entity.website_engagement | engageStatusDotsClass }}"
                          uib-tooltip="{{row.entity.website_engagement}}" tooltip-placement="right" tooltip-append-to-body="true"></span>`
        },
        {
          displayName: 'Account Status',
          name: 'account_status',
          field: 'account_status',
          cellTooltip: false,
          sortingAlgorithm: this.customSort,
          cellClass: '',
          minWidth: 160,
          filter: { 'condition': this.textFilter },
          cellTemplate: `<span class="ui-grid-cell-contents {{row.entity.account_status | engageStatusClass}}">
                          <state-arrow ng-if="row.entity.account_status" state="row.entity.account_status_signal"></state-arrow>
                          {{row.entity.account_status | noneDash}}
                         </span>`
        }
      ],
      onRegisterApi: gridApi => {
        this.$animate.enabled(gridApi.grid.element, false);
        this.gridApi = gridApi;
      }
    };

    this.getOpportunities();

    this.$scope.$on('tsSwitchToggle-internalFilter', this.internalFilterToggle.bind(this));
    this.getUser();
  }

  getOpportunities() {
    let oppsPromise = this.HorizonService.getEngageOpportunities()
    .then(opps => {
      let placeholdertxt = "No more accounts to show";
      let zero_opp = placeholderRow(this.grid.columnDefs, placeholdertxt);
      opps.push(zero_opp);

      this.grid.data = opps;
    });

    this.oppsTracker.addPromise(oppsPromise);
  }

  isActive() {
   	var obj = this.gridApi.columnDefs[index];
	  if (obj.visible === undefined) return true;
	  return obj.visible;
	}

  internalFilterToggle() {
    let filter = this.HorizonService.getInternalFilter();
    filter ? this.HorizonService.disableInternalFilter() : this.HorizonService.enableInternalFilter();

    filter = this.HorizonService.getInternalFilter();
    this.getOpportunities();
  }

  getUser() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }
}
