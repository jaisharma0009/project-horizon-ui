'use strict';

import webpackDev   from './webpack/config/webpack.dev';
import webpackDist  from './webpack/config/webpack.dist';

var fs = require('fs'),
  url = require('url'),
  gulp = require('gulp'),
  gchanged = require('gulp-changed'),
  zip = require('gulp-zip'),
  rimraf = require('rimraf'),
  gutil = require('gulp-util'),
  argv = require('yargs').argv;

var webpack = require('webpack'),
  WebpackDevServer = require('webpack-dev-server');

var config = require('./config');

/**
 * Usage:
 *    gulp serve          Launch the app in a development server and watch for changes
 *    gulp dist           Build the app for distribution
 *        --save-stats    Save webpack stats to webpack.stats.json
 */

gulp.task('webpack', function(done) {
  var webpackConfig = webpackDist({
    env: 'prod'
  });

  webpack(webpackConfig, function(err, stats) {
    if (err) { done(err); }

    gutil.log(stats.toString({
      colors: true
    }));

    if (gutil.env['save-stats']) {
      fs.writeFile(config.paths.dist + '/webpack.stats.' + stats.hash + '.json',
                   JSON.stringify(stats.toJson(), null, 2),
                   done);
    } else {
      done();
    }
  });
});

gulp.task('webpack-dev-server', function (done) {
  let webpackConfig = webpackDev({
      env: argv.env || 'dev'
    })
    , webpackServerConfig = require('./webpack.server.config')
    , serverUrl = url.format({
      protocol: 'http',
      hostname: webpackServerConfig.host,
      port: webpackServerConfig.port
    });

  // add an entry for the websocket client, and HOT if enabled
  var clientEntry = 'webpack-dev-server/client?' + serverUrl;

  if (typeof webpackConfig.entry === 'object' &&
      !Array.isArray(webpackConfig.entry)) {
    webpackConfig.entry['webpack-dev-server'] = clientEntry;

    if (webpackServerConfig.hot) {
      gutil.log('Hot!');
      webpackConfig.entry['webpack-hot-dev-server'] = 'webpack/hot/dev-server';
    }
  } else {
    if (!Array.isArray(webpackConfig.entry)) {
      webpackConfig.entry = [webpackConfig.entry];
    }

    webpackConfig.entry.unshift(clientEntry);

    if (webpackServerConfig.hot) {
      gutil.log('Hot!');
      webpackConfig.entry.unshift('webpack/hot/dev-server');
    }
  }

  new WebpackDevServer(webpack(webpackConfig), webpackServerConfig)
    .listen(webpackServerConfig.port, webpackServerConfig.hostname, function(err) {
      if (err) { done(err); }
      gutil.log('Server running at ' + serverUrl);
    });
});


gulp.task('copy-static', function(done) {

  return gulp.src(config.paths.static + '/**/*',
                  { base: config.paths.static })
    .pipe(gchanged(config.paths.dist))
    .pipe(gulp.dest(config.paths.dist));

});

gulp.task('clean-dist', function(done) {

  return rimraf(config.paths.dist, done);

});


gulp.task('clean-site-dist', function(done) {

  return rimraf(config.paths.siteDist, done);

});

gulp.task('copy-site', function(done){

    return gulp.src(config.paths.dist + '/**/*', { base: config.paths.dist })
      .pipe(gulp.dest(config.paths.siteDist));

});

gulp.task('zip-site', function(done){

  return gulp.src(config.paths.site + '/**/*',{ base: config.paths.site })
        .pipe(zip('../site.zip'))
        .pipe(gulp.dest('../'));
});

gulp.task('dist', [ 'webpack', 'copy-static']);

gulp.task('site', ['clean-site-dist','copy-site','zip-site']);

gulp.task('dist-site', ['dist','site']);

gulp.task('start', ['webpack-dev-server']);

gulp.task('serve', ['webpack-dev-server']);
