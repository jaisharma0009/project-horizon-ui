'use strict';

let _ = require('lodash');
let approx = require('approximate-number');

module.exports = class ExportController {
  constructor($scope, $state, contextService, HorizonService, promiseTracker) {
    'ngInject';

    this.$scope = $scope;
    this.contextService = contextService;
    this.HorizonService = HorizonService;

    this.region = 'WW';
    this.selectedQuarter = 1;
    this.selectedYear = 2016;

    this.overviewsData = {};
    this.quarterPipeline = null;
    this.pipelineInfluence = null;
    this.topCampaigns = [];
    this.lowestCampaigns = [];
    this.eventsInfluenceSummary = {};
    this.eventConversion = null;
    this.eventClosed = null;
    this.eventInfluenced = null;

    this.overviewsTracker = promiseTracker();
    this.topCampaingsTracker = promiseTracker();
    this.eventsTracker = promiseTracker();
  }

  $onInit() {
    this.selectedYear = this.getCurrentQuarter().year;
    this.selectedQuarter = this.getCurrentQuarter().quarter;
    
    this.years = this.getYears(2016);
    this.quarters = this.getQuarters(this.selectedYear);
    this.regions = this.getRegions();

    this.$scope.$watch(() => this.region, this.fetchRegionData.bind(this));
    this.fetchData();
  }

  fetchRegionData() {
    this.getOverviews(this.region, this.selectedQuarter, this.selectedYear);
  }

  fetchData() {
    this.getTopCampaings(null, this.selectedQuarter, this.selectedYear);
    this.getEventsInfluenceSummary(null, this.selectedQuarter, this.selectedYear);
    this.getEventsByType(null, this.selectedQuarter, this.selectedYear);
    this.getEventsByTypeClosed(null, this.selectedQuarter, this.selectedYear);
    this.getEventConvertionScore(null, this.selectedQuarter, this.selectedYear);
  }

  getRegions() {
    return [
      { services_pod: 'WW' },
      { services_pod: 'Americas' },
      { services_pod: 'LATAM' },
      { services_pod: 'EMEA' },
      { services_pod: 'APAC' },
      { services_pod: 'Unassigned' }
    ]
  }

  selectRegion(region) {
    this.region = region;
  }

  getOverviews(region, quarter, year) {
    let promise = this.HorizonService.getOverviews(region, quarter, year)
      .then(data => {
        this.overviewsData = data;
        this.quarterPipeline = this.parseQuarterPipeline(data);
        this.pipelineInfluence = this.parsePipelineInfluence(data);
        this.timeToCloseData = this.parseTimetoClose(data);
      });
    this.overviewsTracker.addPromise(promise);
  }

  parseTimetoClose(data) {
    let quarters = ['q1', 'q2', 'q3', 'q4'];

    return quarters.map((q) => {
      let priorYear = data[`yoy_${q}_time_to_close_days`]
        , currentYear = data[`current_${q}_time_to_close_days`];
      return { priorYear, currentYear };
    });
  }

  parseQuarterPipeline(data) {
    return {
      inquiries: [
        { value: data.current_quarter_inquiry_count, type: 'current', label: "Current Q Leads", goal: data.inquiry_goal },
        { value: data.yoy_quarter_inquiry_count, type: 'prev', label: "Leads YoY" }
      ],
      aql: [
        { value: data.current_aql_count, type: 'current', label: "Current Q AQL", goal: data.aql_goal },
        { value: data.yoy_aql_count, type: 'prev', label: "AQL YoY" },
      ],
      sal: [
        { value: data.current_sal_count, type: 'current', label: "Current Q SAL", goal: data.sal_goal },
        { value: data.yoy_sal_count, type: 'prev', label: "SAL YoY" }
      ],
      sql: [
        { value: data.current_sql_count, type: 'current', label: "Current Q SQL", goal: data.sql_goal },
        { value: data.yoy_sql_count, type: 'prev', label: "SQL YoY" }
      ],
      s2: [
        { value: data.current_s2_count, type: 'current', label: "Current Q S2", goal: data.s2_goal },
        { value: data.yoy_s2_count, type: 'prev', label: "S2 YoY" }
      ]
    }
  }

  parsePipelineInfluence(data) {
    return {
      marketing: [
        { value: data.current_quarter_marketing_influence_amount, type: 'current' },
        { value: data.yoy_quarter_marketing_influence_amount, type: 'prev' }
      ],
      sales: [
        { value: data.current_quarter_sales_influence_amount, type: 'current' },
        { value: data.yoy_quarter_sales_influence_amount, type: 'prev' },
      ],
      partner: [
        { value: data.current_quarter_partner_influence_amount, type: 'current' },
        { value: data.yoy_quarter_partner_influence_amount, type: 'prev' }
      ]
    }
  }

  getTopCampaings(region, quarter, year) {
    let promise = this.HorizonService.getTopCampaings(region, quarter, year)
      .then(data => {
        this.topCampaigns = _.filter(data, { type: "Top 3" });
        this.lowestCampaigns = _.filter(data, { type: "Bottom 3" });
      });

    this.topCampaingsTracker.addPromise(promise);
  }

  getEventsInfluenceSummary(region, quarter, year) {
    let promise = this.HorizonService.getEventsInfluenceSummary(region, quarter, year)
      .then(data => { this.eventsInfluenceSummary = data; });

    this.eventsTracker.addPromise(promise);
  }

  getEventsByType(region, quarter, year) {
    let promise = this.HorizonService.getEventsByType(region, quarter, year)
      .then(data => {
        this.eventInfluenced = this.parseEventsByType(data);
      });

    this.eventsTracker.addPromise(promise);
  }

  getEventsByTypeClosed(region, quarter, year) {
    let promise = this.HorizonService.getEventsByTypeClosed(region, quarter, year)
      .then(data => {
        this.eventClosed = this.parseEventsByType(data);
      });

    this.eventsTracker.addPromise(promise);
  }

  getEventConvertionScore(region, quarter, year) {
    let promise = this.HorizonService.getEventConvertionScore(region, quarter, year)
      .then(data => {
        this.eventConversion = this.parseEventsByType(data);
      });

    this.eventsTracker.addPromise(promise);
  }

  parseEventsByType(events) {
    let grouped = _.groupBy(events, 'eventtype');

    let obj = {};

    _.map(grouped, (item, index) => {
      let elements = [];

      _.each(item, i => {
        let value = i.opportunity_counts;
        if (!value) value = i.conversion_pct;

        let type = i.quarter == 'Current' ? 'current' : 'prev';
        elements.push({ value, type })
      })

      index = index.split(' ').join('_');

      obj[index] = elements;
    });

    return obj;
  }

  getYears(start) {
    let today = new Date();
    let year = today.getFullYear();
    
    let years = [];
    
    for( let i = start; i <= year; i++ ) {
      years.push(i);
    }

    return years;
  }

  getQuarters(year) {
    let today = new Date();
    let currentYear = today.getFullYear();

    if(year == currentYear) {
      let quarters = [];
      let quarter = Math.floor((today.getMonth() + 3) / 3);
      
      for(let i = quarter; i > 0; i -- ) {
        quarters.push(i);
      }

      return quarters;
    }

    return [1, 2, 3, 4];
  }

  getCurrentQuarter() {
    let today = new Date();
    let quarter = Math.floor((today.getMonth() + 3) / 3);
    let year = today.getFullYear();

    return { year, quarter };
  }

  selectQuarter(q) {
    this.selectedQuarter = q;
    this.fetchData();
    this.fetchRegionData();
  }

  selectYear(y) {
    this.selectedYear = y;

    this.quarters = this.getQuarters(y);

    if(!this.quarters.includes(this.selectedQuarter)) {
      this.selectedQuarter = 1;
    }

    this.fetchRegionData();
    this.fetchData();
  }

}
