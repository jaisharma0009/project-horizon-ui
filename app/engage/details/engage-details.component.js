var template = require('./engage-details.html');
var EngageDetailsController = require('./engage-details.controller');

let EngageDetailsComponent = {
  restrict: 'E',
  template,
  controller: EngageDetailsController,
  controllerAs: 'details'
};

module.exports = EngageDetailsComponent;
