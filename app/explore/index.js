'use strict';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default;

module.exports = angular.module('app.explore', [
    angularUiRouter,
    require('../services').name
  ])
  .config(function($stateProvider) {
     $stateProvider
      .state('explore', {
        url: '/explore',
        component: 'explore',
        data: {
          cssClasses: 'narrow-body'
        }
      });
  })
  .component('explore', require('./explore.component'))
  .component('campaigns', require('./campaigns/campaigns.component'))
  .component('barChart', require('./bar-chart/bar-chart.component'))
  .component('hBarChart', require('./h-bar-chart/h-bar-chart.component'))
  .component('qSelect', require('./quarter-select/qselect.component'))
  .component('lineChart', require('./line-chart/line-chart.component'));
