'use strict';

var angular = require('angular'),
  angularMoment = require('angular-moment'),
  angularPromiseTracker = require('angular-promise-tracker'),
  angularUiRouter = require('@uirouter/angularjs').default,
  uiGrid = require('angular-ui-grid').default
  ;

  var section = 'expansion-rating';

  var identifier = 'customers.detail.'+section;  
  var ctrlr_id = identifier.split('.').join('')+'ctrl';
  

module.exports = angular.module('app'+identifier, [
    angularMoment,
    angularPromiseTracker,
    angularUiRouter,
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.pagination',
    'ui.grid.edit',
    require('tsbusy').name,
    require('tsuigrid').name,
    require('jqsparkline').name,
    require('radarchart').name
])
  .config(function($stateProvider) {
    $stateProvider
      .state(identifier, {
        url: '/'+section,
        component: 'expansionRating'
      });
  })
  .component('expansionRating', require('./expansion-rating.component'));

