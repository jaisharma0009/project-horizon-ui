'use strict';

/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.$watch('priority', function(priority) {
    var typeRegExResult = /(?:[0-9]-)?([Cc]ritical|[Hh]igh|[Mm]edium|[Ll]ow)/i.exec(priority);

    $scope.priorityType = typeRegExResult ? typeRegExResult[1].toLowerCase() : null;
  });
};
