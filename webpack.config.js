'use strict';

var path = require('path'),
    url = require('url'),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    NgAnnotatePlugin = require('ng-annotate-webpack-plugin'),
    componentHotLoader = require.resolve('./gulp/loaders/component.loader'),
    htmlHotLoader = require.resolve('./gulp/loaders/html.loader'),
    directiveHotLoader = require.resolve('./gulp/loaders/directives.loader');

var config = require('./config');


module.exports = function (options) {
  options = options || {};

  var webpackConfig = {
    context: path.join(__dirname, config.paths.app),

    entry: ['./app'],

    output: {
      path: path.join(__dirname, config.paths.dist),
      filename: '[name].bundle.[hash].js',
      chunkFilename: '[id].bundle.[chunkhash].js',
      namedChunkFilename: '[name].bundle.[hash].js'
    },

    cache: true,

    resolve: {
      // look for modules installed by both npm and bower
      modulesDirectories: [
        'node_modules',
        'web_modules',
        path.join(config.paths.app, 'components')
      ],

      alias: {
        'angular-bootstrap': 'angular-bootstrap/ui-bootstrap-tpls',
        'angulartics': 'angulartics',
        'angulartics-ga': 'angulartics/src/angulartics-ga',
        'angulartics-gtm': 'angulartics/src/angulartics-gtm',
        'footable': 'footable',
        'footable/footable.sort': 'footable/js/footable.sort',
        'jquery.sparkline': 'jquery-sparkline',
        'uri.js': 'uri.js/src/uri.js'
      }
    },

    plugins: [

      // new webpack.optimize.DedupePlugin(),

      // new webpack.optimize.CommonsChunkPlugin(/* filenameTemplate= */ 'vendor', /* filename= */ 'vendor.bundle.js'),

      new webpack.optimize.OccurenceOrderPlugin( /* preferEntry= */ true),

      // Generate index.html
      new HtmlWebpackPlugin({
        filename: 'index.html',
        inject: true,
        template: path.join(config.paths.app, 'index.template.html')
      }),

      // Extract CSS into a separate file (don't embed it in the JS bundle)
      new ExtractTextPlugin('[name].bundle.[hash].css'),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        'ENV': JSON.stringify(options.env)
      })

    ],

    recordsPath: path.resolve(__dirname, 'webpack.records.json'),

    module: {
      preLoaders: [
        // add component hot loader
        {
          test: [/\.js$/],
          loader: componentHotLoader,
          include: [
            path.resolve(__dirname, 'app')
          ],
          exclude: [/node_modules/, /components/]
        },
        // add directive hot loader
        {
          test: [/\.js$/],
          loader: directiveHotLoader,
          include: [
            path.resolve(__dirname, 'app/components')
          ],
          exclude: [/node_modules/]
        }
      ],
      noParse: [
        // don't scan libraries for require/define calls (they don't have any,
        // unless they use an imports loader)
        /\/highcharts(?:\.src)?\.js$/,
        /\/highstock(?:\.src)?\.js$/,
        /\/moment(?:-with-langs)?(?:\.min)?\.js$/
      ],
      loaders: [
        // NOTE:
        //    the url-loader uses data-urls if file is small enough; else, files.
        //    the file-loader emits files.

        // file-specific
        {
          test: /\/angular(?:\.min)?\.js$/,
          loader: 'exports?angular'
        }, {
          test: /\/angular-file-saver(?:\.min)?\.js$/,
          loader: 'exports?FileSaver'
        }, {
          test: /\/angular-sanitize(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ngSanitize"'
          ]
        }, {
          test: /\/angular-animate(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ngAnimate"'
          ]
        }, {
          test: /\/angular-cookie(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ipCookie"'
          ]
        }, {
          test: /\/angular-cookies(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ngCookies"'
          ]
        }, {
          test: /\/angular-mocks(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?angular.mock'
          ]
        }, {
          test: /\/angular-moment(?:\.min)?\.js$/,
          loaders: [
            'imports?moment=moment',
            'exports?"angularMoment"'
          ]
        }, {
          test: /\/angularjs-gravatardirective(?:.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"angularjs-gravatardirective"'
          ]
        }, {
          test: /\/angular-wizard(?:\.min)?\.js$/,
          loader: 'imports?angular=angular'
        }, {
          test: /\/angularLocalStorage\.js$/,
          loaders: [
            'imports?angular=angular,ngCookies=angular-cookies',
            'exports?"angularLocalStorage"'
          ]
        }, {
          test: /\/footable(?:\.all)?\.js$/,
          loaders: [
            'imports?jQuery=jquery',
            'exports?window.footable'
          ]
        }, {
          test: /.js/,
          include: path.join(__dirname, 'node_modules/jsonp'),
          loaders: ['transform?packageify', 'transform?brfs']
        }, {
          test: /\/highstock(?:\.src)?\.js$/,
          loaders: [
            // 'imports?jQuery=jquery' // satisfied by jQuery expose loader
            'exports?Highcharts,HighchartsAdapter'
          ]
        }, {
          test: /\/highstock\/modules\/(?:[^\/]*)?\.js$/,
          loaders: [
            'imports?Highcharts=>require("highcharts-shim").Highcharts'
          ]
        }, {
          test: /\/highcharts-ng(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular,Highcharts=>require("highcharts-shim").Highcharts',
            'exports?"highcharts-ng"'
          ]
        }, {
          test: /\/promise-tracker\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ajoslin.promise-tracker"'
          ]
        }, {
          test: /\/angulartics\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"angulartics"'
          ]
        }, {
          test: /\/angulartics-ga\.js$/,
          loaders: [
            'imports?angular=angular&angulartics=angulartics',
            'exports?"angulartics.google.analytics"'
          ]
        }, {
          test: /\/angulartics-gtm\.js$/,
          loaders: [
            'imports?angular=angular&angulartics=angulartics',
            'exports?"angulartics.google.tagmanager"'
          ]
        }, {
          test: /\/restangular(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular&_=lodash',
            'exports?"restangular"'
          ]
        }, {
          test: /\/ui-bootstrap(?:-tpls)?(?:\.min)?\.js$/,
          loaders: [
            'imports?angular=angular',
            'exports?"ui.bootstrap"'
          ]
        },
        {
          test: /\/jquery(?:\.min)?\.js$/,
          loaders: [
            'expose?jQuery' // for libraries that expect window.jQuery (e.g. highcharts)
          ]
        },
        {
          test: /\/jquery.sparkline(?:\.min)?\.js$/,
          loaders: [
            'imports?$=jquery',
            'exports?"jquery.sparkline"'
          ]
        },

        // format-specific

        // HTML
        {
          test: /\.html/,
          loader: 'html'
        },

        // CSS
        {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract('style', 'css!autoprefixer')
        },

        // LESS
        {
          test: /\.less$/,
          loader: ExtractTextPlugin.extract('style','css!autoprefixer!less')
        },

        // { test: [/ui-grid\.svg/, /ui-grid\.eot/, /ui-grid\.ttf/, /ui-grid\.woff/, /ui-grid\.woff2/],
        //   use: 'file-loader?name=fonts/[name].[ext]' },
        // webfonts
        {
          test: /\.woff($|\?)/,
          loader: 'url?name=fonts/[hash].[ext]&limit=10000&mimetype=application/font-woff'
        }, {
          test: /\.woff2($|\?)/,
          loader: 'url?name=fonts/[hash].[ext]&limit=10000&mimetype=application/font-woff2'
        }, {
          test: /\.(?:ttf|eot)($|\?)/,
          loader: 'file?name=fonts/[hash].[ext]'
        }, {
          test: /fonts?(?:.*)\.svg($|\?)/,
          loader: 'file?name=fonts/[hash].[ext]'
        },

        // images
        {
          test: /\.(?:png|jpg|gif|svg)($|\?)/,
          loader: 'file?name=images/[hash].[ext]'
        },

        {
          loader: "babel-loader",
          test: /\.js?$/,
          exclude: /(node_modules|bower_components)/,
          include: [
            path.resolve(__dirname, "app"),
          ]
        },
      ],
      postLoaders:  [
        {
          test: [/\.html$/],
          loader: htmlHotLoader,
          include: [
            path.resolve(__dirname, 'app')
          ],
          exclude: [/components/,/node_modules/]
        }
      ]
    }
  };

  if (options.env === 'prod') {
    /*        webpackConfig.plugins.unshift(new webpack.optimize.UglifyJsPlugin({
                compress: {
                    screw_ie8: true
                },
                mangle: {
                    screw_ie8: true
                },
                output: {
                    screw_ie8: true
                }
            }));
    */
    webpackConfig.plugins.unshift(new NgAnnotatePlugin({
      add: true,
      single_quotes: true
    }));

    webpackConfig.plugins.unshift(
      new webpack.ProvidePlugin({
        d3: 'd3',
      })
    );

    webpackConfig.profile = true;
  } else if (options.env === 'dev') {
    webpackConfig.debug = true;

    // webpackConfig.devtool = 'source-map';
    // webpackConfig.profile = true;
  } else if (options.env === 'local') {

  }

  if (typeof options.progress === 'function') {
    var callback = options.progress.bind(webpackConfig);

    webpackConfig.plugins = webpackConfig.plugins || [];
    webpackConfig.plugins.unshift(new webpack.ProgressPlugin(callback));
  }

  return webpackConfig;
};
