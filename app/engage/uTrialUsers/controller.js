'use strict';

let paginationPageSizes = require('../../values.js').paginationSizes;
let customRow = require('../../values.js').customRow;
let placeholderRow = require('../../values.js').placeholderRow;
let customSort = require('../../values.js').customSort;

module.exports = class UTrialUsersController {
  constructor($scope, $state, HorizonService) {
    'ngInject';

    this.$scope = $scope;
    this.HorizonService = HorizonService;
    this.customRow = customRow();
    this.customSort = customSort;
  }

  $onInit() {
    this.grid = {
      paginationPageSize: 100,
      paginationPageSizes: paginationPageSizes.sizes,
      enablePaginationControls: true,
      data: [],
      enableFiltering: true,
      exporterCsvFilename: 'myFile.csv',
      enableGridMenu: false,
      exporterMenuPdf: false,
      enableColumnMenus: false,
      rowHeight: 30,
      rowTemplate: this.customRow,
      columnDefs: [
        {
          displayName: 'Contact name',
          name: 'contact_name',
          field: 'account_name',
          minWidth: 100,
          sortingAlgorithm: this.customSort,
          cellTooltip: true,
          cellTemplate: `<div class="ui-grid-cell-contents"">
                          <span title="{{row.entity.contact_name}}">{{row.entity.contact_name}}</span>
                         </div>`
        },
        {
          displayName: 'Title',
          name: 'title',
          field: 'title',
          sortingAlgorithm: this.customSort
        },
        {
          displayName: 'Company',
          name: 'company',
          field: 'company',
          sortingAlgorithm: this.customSort
        },
        {
          displayName: 'Trial sign-up',
          name: 'trial_signup',
          field: 'trial_signup',
          sortingAlgorithm: this.customSort
        }
      ]
    }

    this.placeholderRow = placeholderRow(this.grid.columnDefs);
    this.getUnassociatedTrialUsers();
  }

  getUnassociatedTrialUsers() {
    this.HorizonService.getUnassociatedTrialUsers()
      .then(users => {
        users.push(this.placeholderRow);
        this.grid.data = users
      });
  }
}
