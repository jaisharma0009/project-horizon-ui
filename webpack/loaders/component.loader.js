'use strict';

var path = require('path');
var utils = require('./utils');

var camelCase = require('lodash').camelCase;

module.exports = function (input) {
  this.cacheable && this.cacheable();

  const fileName = path.basename(this.resourcePath, '.component.js');
  const controllerName = `${utils.capitalize(camelCase(fileName))}Controller`;
  const directiveName = camelCase(utils.getDirectiveName(this.resourcePath));

  if (this.resourcePath.indexOf('.component.js') < 0) {
    return input;
  }

  return input + `
    if (module.hot) {
      module.hot.accept();
      // get controller instance
      const name = ${controllerName}.name;
      // don't do anything if the directive is not printed
      const doc = angular.element(document.body);
      const injector = doc.injector();
      if (injector) {
        const directive = injector.get('${directiveName}Directive')[0];
        if (directive) {
          const origin = directive.controller;
          const target = ${controllerName}.prototype;
          const enumAndNonenum = Object.getOwnPropertyNames(target);
          const enumOnly = Object.keys(target);
          // not found in enumOnly keys mean the key is non-enumerable,
          // so return true so we keep this in the filter if it's not the constructor


          const constructorOnly = enumAndNonenum.find(key => enumOnly.indexOf(key) === -1 && key === 'constructor');
          if(constructorOnly.length > 0) {
            if(origin.prototype['constructor'].toString() !== target['constructor'].toString()){
              location.reload();
            }
          }

          var onInit = false;
          const onInitOnly = enumAndNonenum.find(key => enumOnly.indexOf(key) === -1 && key === '$onInit');
          if(onInitOnly.length > 0) {
            if(origin.prototype['$onInit'].toString() !== target['$onInit'].toString()){
              onInit = true;
            }
          }

          const nonenumOnly = enumAndNonenum.filter(key => enumOnly.indexOf(key) === -1 && key !== 'constructor');
          nonenumOnly.forEach(val => origin.prototype[val] = target[val]);

          // trigger rootscope update
          doc.scope().$apply();

          if(onInit) {
            target['$onInit']();
          }

          console.info('Hot Swapped component ' + name);
        }
      }
    }
  `;
};
