'use strict';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default;

module.exports = angular.module('app.customers', [
    angularUiRouter,
    require('tsauth').name,
    require('./list').name,
    require('./detail').name,
    require('../services').name
  ])
  .config(function($stateProvider) {
    /*$stateProvider
      .state('customers', {
        abstract: true,
        url: '/customers',
        template: '<ui-view/>'
      });*/
  });
