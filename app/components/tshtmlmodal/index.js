'use strict';

let angular = require('angular');
let uib = require('angular-ui-bootstrap');
let register = require('../directive.register.js');

import TsHtmlModalDirective from './ts-html-modal.directive';
/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.tshtmlmodal', ['ui.bootstrap'])
  .directive('tsHtmlModal', register(TsHtmlModalDirective))
