'use strict';

module.exports = class CampaignsController {
  constructor($scope, $state) {
    'ngInject';

    this.general = {
      class: '',
      state: 0,
      title: ""
    }
  }

  $onInit() {

    switch(this.type) {
      case 'top':
        this.general.title = "Top";
        this.general.class = 'panel-title-green';
        this.general.state = 1;
      break;

      case 'lowest':
      this.general.title = "Lowest";
      this.general.class = 'panel-title-red';
      this.general.state = -1;
      break;
    }
  }
}