'use strict';

var angular = require('angular');

/**
 * Directive for authenticating with Apigee SSO
 */
module.exports = angular.module('app.components.tssidenav', [])
  .directive('tsSidenav', function() {
    return {
      restrict: 'E',
      scope: true,
      template: require('./template.html'),
      controller: 'tsSidenavCtrl'
    };
  })
  .controller('tsSidenavCtrl', require('./controller'));