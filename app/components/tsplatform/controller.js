'use strict';

/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.$watch('platform', function(platform) {
    $scope.platformName = null;
    $scope.label = null;

    if (typeof platform === 'string' && platform !== 'Unspecified') {
      if (platform.slice(-1) === '*') {
        $scope.label = '3.8';
        $scope.platformName = platform.slice(0, -1);
      } else {
        $scope.platformName = platform;
      }
    }
  });
};
