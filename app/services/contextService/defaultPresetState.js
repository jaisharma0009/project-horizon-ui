module.exports = {  
   "shortdesc":"All",
   "entity":{  
      "columns":[  
         {  
            "name":"region",
            "visible":true,
            "width":"7%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"services_pod",
            "visible":true,
            "width":"7%",
            "sort":{  

            },
            "filters":[  
               {  
                  "term":null
               }
            ]
         },
         {  
            "name":"account_name",
            "visible":true,
            "width":"15%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"expansion_target",
            "visible":true,
            "width":"7%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"priority",
            "visible":true,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"latest_magellan_score",
            "visible":true,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"latest_magellan_execution",
            "visible":false,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"latest_magellan_vision",
            "visible":false,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"latest_magellan_alignment",
            "visible":false,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"normalized_score",
            "visible":true,
            "width":"*",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"sales_rep_email",
            "visible":true,
            "width":"*",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"industry",
            "visible":false,
            "width":"10%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"pro_rated_percent_of_entitlement",
            "visible":true,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"90d_traffic",
            "visible":true,
            "width":"21%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"support_score",
            "visible":false,
            "width":"5%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"platform",
            "visible":true,
            "width":"12%",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         },
         {  
            "name":"next_close_date",
            "visible":false,
            "width":"*",
            "sort":{  

            },
            "filters":[  
               {  

               }
            ]
         }
      ],
      "scrollFocus":{  

      },
      "selection":[  

      ],
      "grouping":{  
         "grouping":[  

         ],
         "aggregations":[  

         ]
      },
      "treeView":{  

      },
      "pagination":{  
         "paginationCurrentPage":1,
         "paginationPageSize":25
      }
   }
};