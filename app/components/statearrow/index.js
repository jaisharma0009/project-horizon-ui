'use strict';

let template = require('./template.html');

module.exports = angular.module('app.components.statearrow', [])
  .directive('stateArrow', function () {
    return {
      restrict: 'E',
      scope: {
        state: '=',
        type: '@'
      },
      template: template,
      link: function (scope, element, attrs, ctrl) {
        scope.stateClass = 'equals';

        scope.$watch('state', function (newVal, oldVal) {
          if (newVal != null) {
            setClass(newVal);
          }
        });

        function setClass(state) {
          switch (state) {
            case '0':
            case 0:
              scope.stateClass = 'equals';
              break;

            case 'up':
            case '1':
            case 1:
              scope.stateClass = 'fa-arrow-up';
              break;

            case 'down':
            case '-1':
            case -1:
              scope.stateClass = 'fa-arrow-down';
              break;

            default:
              scope.stateClass = state > 0 ? 'fa-arrow-up' : 'fa-arrow-down';
            break;
          }
        }
      }
    };
  });
