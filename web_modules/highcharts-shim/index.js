'use strict';

var highcharts = require('highstock/highstock');
var Highcharts = highcharts.Highcharts;

//require('highstock/highcharts-more');


require('./dateformats')(Highcharts);
//var gray = require('highstock/themes/gray');
require('./theme')(Highcharts);
//require('./theme-dark');


module.exports = highcharts;

require('highstock/modules/exporting');
//require('highstock/modules/no-data-to-display');
