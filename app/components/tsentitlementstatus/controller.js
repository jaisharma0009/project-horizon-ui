'use strict';

var moment = require('moment');


/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.$watch(
    function($scope){
      return $scope.entitled+'|'+$scope.used;
    }, 
    function(updated) {

      $scope.status = 'green';
      $scope.statusType = 'ok';

      if(! $scope.entitled )
        return;

      if(! $scope.used)
        return;

      var used = parseInt($scope.used, 10),
          entitled = parseInt($scope.entitled,10);


      try{

        var today = moment();

        var start = today.clone().startOf('quarter');
        var end = today.clone().endOf('quarter');

        var daysInQtr = end.diff(start, 'days');
        var dayOfQtr = today.diff(start, 'days');

        var percentComplete = dayOfQtr / daysInQtr;

        var entitledToDateInQuarter = entitled * percentComplete;

        var ratio = used/entitledToDateInQuarter;

        $scope.tooltip = 'You have used '+(ratio*100).toFixed(2)+'% of your entitlement to date';

        if(ratio >= 1){
          $scope.status = 'red';
          $scope.statusType = 'danger';
          return;
        } 

        if(ratio >= .75){
          $scope.status = 'yellow';
          $scope.statusType = 'warning';
          return;
        } 

        $scope.status = 'green';
        $scope.statusType = 'ok';
        return;

      }catch(ex){
        console.error(ex);
      }

  });
};
